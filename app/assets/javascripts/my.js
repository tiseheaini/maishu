
String.prototype.trim=function()
{
     return this.replace(/(^\s*)(\s*$)/g,'');
};
(function(window){
	function myJs(name){
		this.is_delete = "确定删除";
	    this.api_url = $('#api_url').val() + '&do=';
	    this.clientCheck = {'name':false,'url':false,'order':false};
	    this.serverCheck = {'name':false,'host':false,'port':false,'order':false};
	};
	 
	myJs.prototype = {  
		isDelete : function (tips)
		{
		    if (confirm(this.is_delete + tips + '?'))
		    {
		        return true;
		    }
		    return false;
		},
		setTimeout : function ()
		{
			setTimeout("$('.alert').css('display','none')",3000);
		},
		popover : function ()
		{
			$('.ycc_popover').each(
				function ()
				{
					$(this).popover('hide');
				}
			);
		},
		porderDetail : function ()
		{
			$('.porder_detail').each(
				function ()
				{
					$(this).click(function(){
						$(this).parent().next().toggle();
						var icon_chevron_class = $(this).find('.icon-chevron').attr('class');
						if(icon_chevron_class.indexOf('icon-chevron-down') !== -1)
						{
							$(this).find('.icon-chevron').removeClass('icon-chevron-down');
							$(this).find('.icon-chevron').addClass('icon-chevron-up');
						}
						else
						{
							$(this).find('.icon-chevron').removeClass('icon-chevron-up');
							$(this).find('.icon-chevron').addClass('icon-chevron-down');
						}
					});
				}
			);
		},
		deletePic : function (t)
		{
			src = $(t).prev().attr('src');
			$(t).parent().remove();
		},
		cookie : function (t)
		{
			 var thing = { plugin: $(t).val()};
			
			var encoded = $.toJSON( thing );
			$.post("index.php?m=api&a=add_books", { data: encoded } ,function callback(json){
				arr = $.evalJSON( json );
				$('.footer').text(arr.plugin);
			});

		},
		setCart : function (id)
		{
			
			var cart_str = $.cookie('cart');
			var cart_arr = {};
			if(cart_str != undefined && cart_str != -1)
			{
				cart_arr = $.evalJSON(cart_str);
			}
			if(id == null)
			{
				if(cart_str == undefined || cart_str == '-1')
				{
					my_js.cartLi();
					$('#cart_content_bar_right_booknum').text(0);
					$('#cart_content_bar_right_price').text(0);
					return;
				}
				$.post("index.php?m=api&a=getBooksInfo", {cookie:$.cookie('cart')} ,function callback(json){
					arr = $.evalJSON( json );
					$('#cart_content_bar_right_booknum').text(arr['total']['booknum']);
					$('#cart_content_bar_right_price').text(arr['total']['price']);
					my_js.cartLi();
					for (var i in arr['list'])
					{
						my_js.cartLi(arr['list'][i]);
					}
				});
				return;
			}
			var book_num = parseInt($('#center_books_li_num_' + id).val());
			var book_price = parseFloat($('#center_books_li_price_' + id).text());
			if (cart_arr[id] != undefined)
			{
				cart_arr[id] = parseInt(cart_arr[id]) +parseInt($('#center_books_li_num_' + id).val());
				var cart_li_num = parseInt($('#cart_content_list_con_num_' + id).val());
				var cart_li_price = parseFloat($('#cart_content_list_con_allprice_' + id).text());
				$('#cart_content_list_con_num_' + id).val(book_num + cart_li_num);
				$('#cart_content_list_con_allprice_' + id).text((book_price * book_num + cart_li_price).toFixed(2));
			}
			else
			{
				
				cart_arr[id] = $('#center_books_li_num_' + id).val();
				var book = new Array();
				book['id'] = id;
				book['name'] = $('#center_books_li_name_' + id).text();
				book['price'] = $('#center_books_li_price_' + id).text();
				book['num'] = parseInt(cart_arr[id]);
				book['all_price'] = parseFloat(book['price']) * parseFloat(book['num']);
				book['all_price'] = book['all_price'].toFixed(2);
				this.cartLi(book);
			}
			var cart_all_num = parseInt($('#cart_content_bar_right_booknum').text());
			var cart_all_price = parseFloat($('#cart_content_bar_right_price').text());
			$('#cart_content_bar_right_booknum').text(book_num + cart_all_num);
			$('#cart_content_bar_right_price').text((book_price * book_num + cart_all_price).toFixed(2));
			$.cookie('cart', $.toJSON(cart_arr), { expires: 1000 * 60 });
		},
		setCartTotal : function (book_num,book_price)
		{
			var cart_all_num = parseInt($('#cart_content_bar_right_booknum').text());
			var cart_all_price = parseFloat($('#cart_content_bar_right_price').text());
			$('#cart_content_bar_right_booknum').text(book_num + cart_all_num);
			$('#cart_content_bar_right_price').text((book_price * book_num + cart_all_price).toFixed(2));
		},
		cartLi : function (book)
		{
			if(book == null)
			{
				$('#cart_content_list_con').text('');
				return ;
			}
			var html = '<tr>';
			html += '<td>' + book['name'] + '</td>';
			html += '<td>' + book['price'] + '</td>';
			html += '<td ><input id="cart_content_list_con_num_'+ book['id'] + '" type="text" style="width:36px;height:16px;margin:0px;" value ="' + book['num'] + '" />&nbsp;<i class="pointer icon-ok" data-id="'+ book['id'] + '" onclick="my_js.cartChNum(this);"></i></td>';
			html += '<td id="cart_content_list_con_allprice_'+ book['id'] + '">' + book['all_price'] + '</td>';
			html += '<td><i onclick="my_js.deleteThis(this)" data-id="'+book['id']+'" class="icon-trash" style="cursor:pointer"></i>' + '</td>';
			html += '</tr>';
			$('#cart_content_list_con').append(html);
		},
		cartChNum : function(t)
		{
        	var id = $(t).attr('data-id');
        	var cart_str = $.cookie('cart');
			var cart_arr = cart_arr = $.evalJSON(cart_str);
			cart_arr[id] = parseInt( $('#cart_content_list_con_num_' + id).val() );
			$.cookie('cart', $.toJSON(cart_arr), { expires: 1000 * 60 });
			
        	my_js.setCart();
           	ui.success('修改成功！');
		},
		deleteThis : function(t)
		{
			ui.confirm('确认删除？',function(z){
		        if(z){
		        	var cart_str = $.cookie('cart');
					var cart_arr = cart_arr = $.evalJSON(cart_str);
					delete cart_arr[$(t).attr('data-id')];
					$.cookie('cart', $.toJSON(cart_arr), { expires: 1000 * 60 });
					
		        	my_js.setCart();
		           	ui.success('删除成功！');    
		        }else{
		            ui.alert('没有删除！');      
		        }
			},true);
		},
		porder : function ()
		{
			var data = {};
			data['name'] = $('#name').val();
			data['address'] = $('#address').val();
			data['tel'] = $('#tel').val();
			data['detail'] = $.cookie('cart');
			$.post("index.php?m=api&a=porder", { data: $.toJSON(data) } ,function callback(json){
				alert(json);
				arr = $.evalJSON( json );
				$('.footer').text(arr.plugin);
			});
		},
		chUni :　function (v,pp)
		{
			var data = {};
			data['id'] = v;
			$.post("index.php?m=api&a=chUni", data ,function callback(json){
				var arr = $.evalJSON( json );
				var college = $('#college');
				college.empty();
				if(pp)
				{
					college.append("<option value='-1'>请选择</option>");
				}
				for (var i in arr['college'])
				{
					college.append("<option value='" + arr['college'][i]['id'] + "'>" + arr['college'][i]['catname'] + "</option>");
				}
				var major = $('#major');
				major.empty();
				if(pp)
				{
					major.append("<option value='-1'>请选择</option>");
				}
				for (var i in arr['major'])
				{
					major.append("<option value='" + arr['major'][i]['id'] + "'>" + arr['major'][i]['catname'] + "</option>");
				}
			});
		},
		chCollege :　function (v,pp)
		{
			var data = {};
			data['id'] = v;
			$.post("index.php?m=api&a=chCollege", data ,function callback(json){
				var arr = $.evalJSON( json );
				var major = $('#major');
				major.empty();
				if(pp)
				{
					major.append("<option value='-1'>请选择</option>");
				}
				for (var i in arr['major'])
				{
					major.append("<option value='" + arr['major'][i]['id'] + "'>" + arr['major'][i]['catname'] + "</option>");
				}
			});
		},
		clrAll : function ()
		{
			ui.confirm('确认清空购物车？',function(z){
		        if(z){
		        	$.cookie('cart', -1);
		        	my_js.setCart();
		           	ui.success('清空成功！');    
		        }else{
		            ui.alert('没有清空！');      
		        }
			},true);
		},
		selectUni : function(t)
		{
			if($(t).css('position') == 'absolute')
			{
				$('#h_s_uni').val(0);
				var left = $.cookie('uni_x');
				$(t).animate({left: left + "px"}, function (){
					$(t).css('position', 'relative');
					$(t).css('left', 0);
					$(t).siblings().each(function ()
					{
					    $(this).show();
					});
					$('.selections').text('');
					$('.select-wrapper').hide(500);
				});
				return;
			}
			$('#h_s_uni').val( $(t).attr('id') );
			var fh = $(t).parent().height();
			var left = $(t).position().left;
			$.cookie('uni_x', left);
			$(t).siblings().each(function ()
			{
			    $(this).hide();
			});
			$(t).parent().css('height', fh);
			$(t).css('position', 'absolute');
			$(t).css('left', left);
			$(t).animate({left:"270px"}, function (){
				$.post("http://www.1haoshudian.com/index.php?m=api&a=index", {cid : $(t).attr('data-id')} ,function callback(json){
					var arr = $.evalJSON( json );
					var c = '';
					for (var i in arr)
					{
						c += '<li><a href="index.php?m=index&a=major&cid=' + arr[i]['id'] + '">' + arr[i]['catname'] + "</a></li>";
					}
					var ul = '<ul style="margin:0;padding:0;" class="step2_select_panel">' + c + '</ul>';
					$('.selections').append(ul);
					$('.select-wrapper').show(500);
				});
			});
		},
		backSelectUni : function()
		{
			var id = $('#h_s_uni').val();
			var t = '#' + id;
			$('#h_s_uni').val(0);
			var left = $.cookie('uni_x');
			$(t).animate({left: left + "px"}, function (){
				$(t).css('position', 'relative');
				$(t).css('left', 0);
				$(t).siblings().each(function ()
				{
				    $(this).show();
				});
				$('.selections').text('');
				$('.select-wrapper').hide(500);
			});
		},
		selectReverse : function()
		{
            $("input[name='select_order[]']").each(
            	function(idx, item) {
                $(item).attr("checked", !$(item).attr("checked"));
            })
        },
	};
	window.my_js = new myJs();
})(window);
class BooksController < ApplicationController
  # GET /books
  # GET /books.json
  before_filter :get_user
  def index
    # @books = Book.order('number').uniq.paginate(:page => params[:page], :per_page => 30)
    @books = Book.paginate(:page => params[:page], :per_page => 30)
  end

  def search
    query = params[:query].strip
    @querys = Book.where("name like ? or author like ? or isbn like ? or publishing_house like ?", "%#{query}%", "%#{query}%", "%#{query}%", "%#{query}%") || []
  end

  # GET /books/1
  # GET /books/1.json
  def show
    @book = Book.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @book }
    end
  end

  # GET /books/new
  # GET /books/new.json
  def new
    if @user.has_role? :admin
      @book = Book.new

      respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @book }
    end
  else
    redirect_to root_path
  end
end

  # GET /books/1/edit
  def edit
    @book = Book.find(params[:id])
  end

  # POST /books
  # POST /books.json
  def create
    @book = Book.new(params[:book])

    respond_to do |format|
      if @book.save
        format.html { redirect_to @book, notice: 'Book was successfully created.' }
        format.json { render json: @book, status: :created, location: @book }
      else
        format.html { render action: "new" }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /books/1
  # PUT /books/1.json
  def update
    @book = Book.find(params[:id])

    respond_to do |format|
      if @book.update_attributes(params[:book])
        format.html { redirect_to @book, notice: 'Book was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book = Book.find(params[:id])
    @book.destroy

    respond_to do |format|
      format.html { redirect_to books_url }
      format.json { head :no_content }
    end
  end

  def import
    file = params[:file].path
    spreadsheet = Roo::Excelx.new(file, nil, :ignore)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      book = Book.new
      book.number = spreadsheet.row(i)[3]
      book.name = spreadsheet.row(i)[4]
      book.isbn = spreadsheet.row(i)[5]
      book.author = spreadsheet.row(i)[6]
      book.price = spreadsheet.row(i)[7]
      book.total = spreadsheet.row(i)[8]
      book.left = spreadsheet.row(i)[9]
      book.publishing_house = spreadsheet.row(i)[10]
      book.save!
    end
    redirect_to root_url, notice: "Books"
  end

  def who_bought
    @book = Book.find(params[:id])
    respond_to do |format|
      format.atom
    end
  end

  private
  def get_user
    if current_user
      @user = current_user
    else
      @user = User.new
    end
  end
end

class OrdersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :get_user
  # GET /orders
  # GET /orders.json
  def all
    @orders = Order.all
  end

  def search
    if params[:query]
      query = params[:query].strip
      @orders = Order.where("id like ?", query)
    else
      @orders = current_user.orders
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
    # render layout: nil
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id])
    @line_items = @order.line_items

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    @cart = current_cart
    if @cart.line_items.empty?
      redirect_to book_url, notice: "您的购物车为空"
      return
    end

    @order = Order.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
  end

  # POST /orders
  # POST /orders.json
  def create
    flash[:notice] = []
    @order = Order.new(params[:order])
    @order.add_line_items_from_cart(current_cart)
    @order.user_id = current_user.id
    @order.address = current_user.address
    @order.line_items.each do |item|
      book = Book.find(item.book_id)
      if (book.left - item.quantity) < 0
        flash[:notice] << "#{ book.name }库存不足"
      end
    end
    
    if flash[:notice].present?
      redirect_to cart_path(session[:cart_id])
      return true
    end
    respond_to do |format|
      if @order.save
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        @order.line_items.each do |item|
          Book.reduce(item.book_id, item.quantity)
        end
        format.html { redirect_to books_path, notice: '感谢您的订单!' }
        format.json { render json: @order, status: :created,location: @order }
      else
        @cart = current_cart
        format.html { render action: "new" }
        format.json { render json: @order.errors,status: :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])

    respond_to do |format|
      if @order.update_attributes(params[:order])
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to search_orders_url }
      format.json { head :no_content }
    end
    # redirect_to all_orders_path , notice: "订单已删除"
  end

  private
  def get_user
    @user = current_user
  end
end

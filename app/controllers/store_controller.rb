class StoreController < ApplicationController
	def index
		@search = Book.search(params[:q])
		@books = @search.result.order('number').uniq.paginate(:page => params[:page], :per_page => 30)
		@user = current_user
		render layout: nil
	end
end

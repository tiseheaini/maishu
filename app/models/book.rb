class Book < ActiveRecord::Base
	attr_accessible :author, :isbn, :left, :name, :number, :price, :publishing_house, :total

	validates_presence_of :name

	has_many :line_items
	has_many :orders, through: :line_items

	before_destroy :ensure_not_referenced_by_any_line_item

  def self.reduce(id, quantity)
    update_left =	Book.find(id).left - quantity
  	Book.update(id, left: update_left )
  end

	def self.to_csv(options = {})
		CSV.generate(options) do |csv|
			csv << column_names
			all.each do |book|
				csv << book.attributes.values_at(*column_names)
			end
		end
	end

	private

    # ensure that there are no line items referencing this book
    def ensure_not_referenced_by_any_line_item
      if line_items.empty?
        return true
      else
        errors.add(:base, 'Line Items present')
        return false
      end
    end

end

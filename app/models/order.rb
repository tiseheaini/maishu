class Order < ActiveRecord::Base
	belongs_to :user
	has_many :line_items, dependent: :destroy
	attr_accessible :address, :email, :name, :telephone, :status

	validates :address, :telephone,  presence: true

	STATUS = [ "", "等待发货中", "发货中", "购书已完成" ]

	def add_line_items_from_cart(cart)
		cart.line_items.each do |item|
			item.cart_id = nil
			line_items << item
		end
	end

	def total_price
		line_items.to_a.sum { |item| item.total_price }
	end
end

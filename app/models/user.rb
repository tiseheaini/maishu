class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, 
  :name, :telephone, :address, :school
  # attr_accessible :title, :body

  # validates_presence_of :name
  validates_uniqueness_of :email, :case_sensitive => false

  has_many :orders
end

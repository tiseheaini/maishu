Maishu::Application.routes.draw do
  resources :orders do
    get 'search', :on => :collection
    post 'search', :on => :collection
    get 'all', :on => :collection
    post 'all', :on => :collection
  end

  resources :line_items


  resources :carts


  devise_for :users

  get "store/index"

  resources :books do
    post 'import', :on => :collection
    get 'search', :on => :collection
    post 'search', :on => :collection
    get :who_bought, on: :member
  end

  root :to => 'store#index'

end

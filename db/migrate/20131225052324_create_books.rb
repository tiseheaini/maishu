class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.integer :number
      t.string :name
      t.string :isbn
      t.string :author
      t.integer :price
      t.integer :total
      t.integer :left
      t.string :publishing_house

      t.timestamps
    end
  end
end
